namespace Equifax
{
    public class PUse
    {
        public static PUse Merge(PUse a, PUse b)
        {
            return new PUse(
                a.UsedInQuerySelect || b.UsedInQuerySelect,
                a.UsedInQueryConstraint || b.UsedInQueryConstraint,
                a.UsedInQuerySequence || b.UsedInQuerySequence,
                a.UsedInActionInput || b.UsedInActionInput,
                a.UsedInActionOutput || b.UsedInActionOutput,
                a.UsedInLookupCondition || b.UsedInLookupCondition,
                a.RequiredInActionInput || b.RequiredInActionInput
                );
        }
        // These two are closely tied to this USE CASE
        // We are going to marry them and result with a merger
        public static PUse StandardQueryInput = new PUse(false, true, false, false, false, false, false);
        public static PUse StandardQueryOut = new PUse(true, false, false, false, false, false, false);
        public PUse(bool qs, bool qc, bool qseq, bool ai, bool ao, bool lc, bool req)
        {
            this.UsedInQuerySelect = qs;
            this.UsedInQueryConstraint = qc;
            this.UsedInQuerySequence = qseq;
            this.UsedInActionInput = ai;
            this.UsedInActionOutput = ao;
            this.UsedInLookupCondition = lc;
            this.RequiredInActionInput = req;
        }
        public bool UsedInQuerySelect { get; }

        public bool UsedInQueryConstraint { get; }

        public bool UsedInActionInput { get; }

        public bool UsedInActionOutput { get; }

        public bool UsedInLookupCondition { get; }

        public bool UsedInQuerySequence { get; }

        public bool RequiredInActionInput { get; }
    }
}