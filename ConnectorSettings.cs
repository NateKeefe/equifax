﻿
namespace Equifax
{
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "E52C9775-371F-11CF-AA6A-0020AF3115BE";
        public const string ConnectorVersion = "1.0.0";
        public const string Description = "Equifax";
        public const string Name = "Equifax";
        public const bool SupportsCloud = true;
        public const string CompanyName = "Equifax";
        public const string AppName = "Equifax";
        public const string Copyright = "Copyright © 2016 ScribeLabs All rights reserved.";

        public const string cryptoKey = "56303022-055B-43E3-9002-7D9264996019";
    }
}
