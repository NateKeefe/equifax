namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using CDK.Entities;

    using Equifax_SOAP.Equifax;

    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Metadata;

    public class MetadataProvider : IMetadataProvider
    {
        private IEnumerable<IActionDefinition> actionDefinitions;
        private IEnumerable<IObjectDefinition> objectDefinitions;
        private IEnumerable<IActionDefinition> ActionDefinitions { get { return this.actionDefinitions ?? (this.actionDefinitions = this.GetActionDefinitions()); } }
        private IEnumerable<IObjectDefinition> ObjectDefinitions { get { return this.objectDefinitions ?? (this.objectDefinitions = this.GetObjectDefinitions()); } }

        public Connector Connector;
        
        public MetadataProvider(Connector connector)
        {
            Connector = connector;
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            return this.ActionDefinitions;
        }

        private IEnumerable<IActionDefinition> GetActionDefinitions()
        {
            return new List<IActionDefinition>
                   {
                        new ActionDefinition
                       {
                            Description = "Query",
                            FullName = "Query",
                            KnownActionType = KnownActions.Query,
                            Name = "Query",
                            SupportsBulk = false,
                            SupportsConstraints = true,
                            SupportsInput = true,
                            SupportsLookupConditions = true,
                            SupportsMultipleRecordOperations = true,
                            SupportsRelations = true,
                            SupportsSequences = true
                       },
                   };
        }

        private IEnumerable<IObjectDefinition> GetObjectDefinitions()
        {
            var objects = new List<IObjectDefinition>();

            var discovery = DiscoveryE.Describe(this.ActionDefinitions.Select(a => a.FullName));
            var details = DetailE.Describe(this.ActionDefinitions.Select(a => a.FullName));

            objects.Add(discovery);
            objects.Add(details);
            return objects;
        }        

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions;
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions.FirstOrDefault(od => od.FullName == objectName);
        }

        public void ResetMetadata()
        {
            // reset metadata
            this.actionDefinitions = this.GetActionDefinitions();
            this.objectDefinitions = this.GetObjectDefinitions();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new System.NotImplementedException();
        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new System.NotImplementedException();
        }
        
        public void Dispose()
        {
        }

        public static string SillyReflectHelper(Type t, string varName, string propsName)
        {
            var sb = new StringBuilder();
            var props = t.GetProperties().Select(p => p.Name);
            foreach (var prop in props)
            {
                var template =
                    $"            if (fields.Contains(\"{prop}\")) {propsName}.Add(\"{prop}\", {varName}.{prop});";
                sb.AppendLine(template);
            }
            return sb.ToString();
        }
    }
}