namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Scribe.Core.ConnectorApi.Metadata;

    public static class PropertyExt
    {
        public static IObjectDefinition MakeCompoundType(
            Type input,
            Type output,
            string name,
            IEnumerable<string> actions)
        {
            var supported = new List<Type>() { typeof(string), typeof(int), typeof(bool), typeof(DateTime), typeof(Guid) };
            var inputPropertiesAll = 
                input.GetProperties();
            var inputProperties =
                    inputPropertiesAll
                    .Where(fi => supported.Contains(fi.PropertyType) )
                    .Where(pi => pi.CanRead && pi.CanWrite)
                    .Select(ToQueryInput).ToList();

            var inputFields = input.GetFields().Where(i => i.IsPublic).Select(ToQueryInput);
            var inputP = GroupJoin(inputProperties, inputFields).ToList();

            var outputProperties = 
                output.GetProperties()
                    .Where(pi => supported.Contains(pi.PropertyType))
                    .Where(pi => pi.CanRead && pi.CanWrite)
                    .Select(ToQueryOutput).ToList();
            var outputFields = output.GetFields().Where(i => i.IsPublic).Select(ToQueryOutput).ToList();

            var outputP = GroupJoin(outputProperties, outputFields);


            var combined = GroupJoin(inputP, outputP );

            var props = combined.Select(ToPropDef).ToList();

            return new ObjectDef(name, "This is a description of " + name + ".", props, actions);
            
        }

        private static IEnumerable<T> Merge<T, TKey>(IEnumerable<T> source, IEnumerable<T> target, Func<T, TKey> keySelector, Func<T, T, T> merge)
        {
            var dict = source.ToDictionary(keySelector, x => x);
            var dict2 = target.ToDictionary(keySelector, x => x);
            foreach (var kv in dict)
            {
                T x2;
                if (dict2.TryGetValue(kv.Key, out x2))
                {
                    yield return merge(kv.Value, x2);
                }
                else
                {
                    yield return kv.Value;
                }
            }
            foreach (var kv in dict2)
            {
                if (!dict.ContainsKey(kv.Key))
                {
                    yield return kv.Value;
                }
            }
        }

        public static IEnumerable<PropDefI> GroupJoin(IEnumerable<PropDefI> a, IEnumerable<PropDefI> b)
        {
            return Merge(a, b, p => p.Name, PropDefI.Combine);
        }

        private static IPropertyDefinition ToPropDef(PropDefI pd)
        {
            return new PropDef(pd.Name, pd.PType, pd.Use) as IPropertyDefinition;
        }

        private static PropDefI ToQueryInput(FieldInfo arg)
        {
            var name = arg.Name;
            var t = arg.FieldType;

            return new PropDefI(name, t, PUse.StandardQueryInput);
        }

        private static PropDefI ToQueryInput(PropertyInfo arg)
        {
            var name = arg.Name;
            var t = arg.PropertyType;

            return new PropDefI(name, t, PUse.StandardQueryInput);
        }

        private static PropDefI ToQueryOutput(FieldInfo arg)
        {
            var name = arg.Name;
            var t = arg.FieldType;

            return new PropDefI(name, t, PUse.StandardQueryOut);
        }

        private static PropDefI ToQueryOutput(PropertyInfo arg)
        {
            var name = arg.Name;
            var t = arg.PropertyType;

            return new PropDefI(name, t, PUse.StandardQueryOut);
        }
    }
}