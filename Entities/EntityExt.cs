﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDK.Entities
{
    using Equifax;

    using Equifax_SOAP.Equifax;

    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Metadata;
    using Scribe.Core.ConnectorApi.Query;

    public static class DiscoveryE
    {
        public const string EntityName = "Discovery";
        public static DataEntity DiscoveryResultToDataEntity(DiscoveryResult r, IList<string> fields)
        {
            var props = new EntityProperties();
            if (fields.Contains("Address")) props.Add("Address", r.Address);
            if (fields.Contains("Best_Match")) props.Add("Best_Match", r.Best_Match);
            if (fields.Contains("City")) props.Add("City", r.City);
            if (fields.Contains("CompanyName")) props.Add("CompanyName", r.CompanyName);
            if (fields.Contains("Country_ISO3")) props.Add("Country_ISO3", r.Country_ISO3);
            if (fields.Contains("EFX_ID")) props.Add("EFX_ID", r.EFX_ID);
            if (fields.Contains("Legal_Entity_Parent_EFX_ID")) props.Add("Legal_Entity_Parent_EFX_ID", r.Legal_Entity_Parent_EFX_ID);
            if (fields.Contains("Match_Rate")) props.Add("Match_Rate", r.Match_Rate);
            if (fields.Contains("Phone")) props.Add("Phone", r.Phone);
            if (fields.Contains("Match_String")) props.Add("Match_String", r.Match_String);
            if (fields.Contains("State_Region")) props.Add("State_Region", r.State_Region);
            if (fields.Contains("Ult_EFX_ID")) props.Add("Ult_EFX_ID", r.Ult_EFX_ID);
            if (fields.Contains("Postal_Code")) props.Add("Postal_Code", r.Postal_Code);

            return new DataEntity(EntityName) { Properties = props };
        }

        public static DiscoveryRequest CreateDiscoveryRequest(IDictionary<string, object> input, AuthHeader authHeader)
        {
            var inp = input;
            var Company_Name = EntityExt.Get<string>(inp, "Company_Name");
            var Address = EntityExt.Get<string>(inp, "Address");
            var City = EntityExt.Get<string>(inp, "City");
            var State_Region = EntityExt.Get<string>(inp, "State_Region");
            var Postal_Code = EntityExt.Get<string>(inp, "Postal_Code");
            var Phone = EntityExt.Get<string>(inp, "Phone");
            var Country = EntityExt.Get<string>(inp, "Country");
            var Large_Business_Indicator = EntityExt.Get<string>(inp, "Large_Business_Indicator");
            var Min_Conf = EntityExt.Get(inp, "Min_Conf", 1);
            //var Min_Conf = EntityExt.Get<int>(inp, "Min_Conf");
            var Postal_Validation = EntityExt.Get<string>(inp, "Postal_Validation");
            var Return_Input_Address = EntityExt.Get<string>(inp, "Return_Input_Address");
            var request = new DiscoveryRequest(
                authHeader,
                Company_Name,
                Address,
                City,
                State_Region,
                Postal_Code,
                Phone,
                Country,
                Large_Business_Indicator,
                Min_Conf,
                Postal_Validation,
                Return_Input_Address);
            return request;
        }

        public static DiscoveryResponse Get(EquifaxCISDMWebServicesSoapClient client, DiscoveryRequest request)
        {
            return client.Discovery(request);
        }

        public static IEnumerable<DataEntity> ToDataEntities(this DiscoveryResponse response, IList<string> fields)
        {
            return response.DiscoveryResult.Select(r => DiscoveryResultToDataEntity(r, fields));
        }

        public static IObjectDefinition Describe(IEnumerable<string> actions)
        {
            var requestType = typeof(DiscoveryRequest);
            var responseType = typeof(DiscoveryResult);
            return PropertyExt.MakeCompoundType(
                requestType,
                responseType,
                EntityName,
                actions);
        }
    }

    public static class DetailE
    {

        public const string EntityName = "Detail";
        public static DataEntity DetailResultToDataEntity(Detail d, IList<string> fields)
        {
            var props = new EntityProperties();

            if (fields.Contains("EFX_ID")) props.Add("EFX_ID", d.EFX_ID);
            if (fields.Contains("Retired_EFX_ID")) props.Add("Retired_EFX_ID", d.Retired_EFX_ID);
            if (fields.Contains("Company_Name")) props.Add("Company_Name", d.Company_Name);
            if (fields.Contains("Legal_Name")) props.Add("Legal_Name", d.Legal_Name);
            if (fields.Contains("Addresses")) props.Add("Addresses", d.Addresses);
            if (fields.Contains("Company_Phone")) props.Add("Company_Phone", d.Company_Phone);
            if (fields.Contains("Company_Fax")) props.Add("Company_Fax", d.Company_Fax);
            if (fields.Contains("Owner_Gender")) props.Add("Owner_Gender", d.Owner_Gender);
            if (fields.Contains("Owner_Ethnicity")) props.Add("Owner_Ethnicity", d.Owner_Ethnicity);
            if (fields.Contains("Primary_Contact")) props.Add("Primary_Contact", d.Primary_Contact);
            if (fields.Contains("Exectives")) props.Add("Exectives", d.Exectives);
            if (fields.Contains("Principal_Owners")) props.Add("Principal_Owners", d.Principal_Owners);
            if (fields.Contains("Primary_Business_Description")) props.Add("Primary_Business_Description", d.Primary_Business_Description);
            if (fields.Contains("Additional_Business_Descriptions")) props.Add("Additional_Business_Descriptions", d.Additional_Business_Descriptions);
            if (fields.Contains("Primary_Narrative_Description")) props.Add("Primary_Narrative_Description", d.Primary_Narrative_Description);
            if (fields.Contains("Additional_Narrative_Descriptions")) props.Add("Additional_Narrative_Descriptions", d.Additional_Narrative_Descriptions);
            if (fields.Contains("Annual_Sales")) props.Add("Annual_Sales", d.Annual_Sales);
            if (fields.Contains("Certifications")) props.Add("Certifications", d.Certifications);
            if (fields.Contains("Classifications")) props.Add("Classifications", d.Classifications);
            if (fields.Contains("Self_Classifications")) props.Add("Self_Classifications", d.Self_Classifications);
            if (fields.Contains("Primary_NAICS")) props.Add("Primary_NAICS", d.Primary_NAICS);
            if (fields.Contains("Additional_NAICS")) props.Add("Additional_NAICS", d.Additional_NAICS);
            if (fields.Contains("Primary_SIC")) props.Add("Primary_SIC", d.Primary_SIC);
            if (fields.Contains("Additional_SIC")) props.Add("Additional_SIC", d.Additional_SIC);
            if (fields.Contains("Ult_Parent_AT_Key")) props.Add("Ult_Parent_AT_Key", d.Ult_Parent_AT_Key);
            if (fields.Contains("Ult_Parent_Name")) props.Add("Ult_Parent_Name", d.Ult_Parent_Name);
            if (fields.Contains("Ult_Parent_Street")) props.Add("Ult_Parent_Street", d.Ult_Parent_Street);
            if (fields.Contains("Ult_Parent_City")) props.Add("Ult_Parent_City", d.Ult_Parent_City);
            if (fields.Contains("Ult_Parent_State")) props.Add("Ult_Parent_State", d.Ult_Parent_State);
            if (fields.Contains("Ult_Parent_Zip")) props.Add("Ult_Parent_Zip", d.Ult_Parent_Zip);
            if (fields.Contains("Ult_Parent_Phone")) props.Add("Ult_Parent_Phone", d.Ult_Parent_Phone);
            if (fields.Contains("Ult_Parent_Country_Code")) props.Add("Ult_Parent_Country_Code", d.Ult_Parent_Country_Code);
            if (fields.Contains("Imm_Parent_AT_Key")) props.Add("Imm_Parent_AT_Key", d.Imm_Parent_AT_Key);
            if (fields.Contains("Imm_Parent_Name")) props.Add("Imm_Parent_Name", d.Imm_Parent_Name);
            if (fields.Contains("Imm_Parent_Street")) props.Add("Imm_Parent_Street", d.Imm_Parent_Street);
            if (fields.Contains("Imm_Parent_City")) props.Add("Imm_Parent_City", d.Imm_Parent_City);
            if (fields.Contains("Imm_Parent_State")) props.Add("Imm_Parent_State", d.Imm_Parent_State);
            if (fields.Contains("Imm_Parent_Zip")) props.Add("Imm_Parent_Zip", d.Imm_Parent_Zip);
            if (fields.Contains("Imm_Parent_Phone")) props.Add("Imm_Parent_Phone", d.Imm_Parent_Phone);
            if (fields.Contains("Imm_Parent_Country_Code")) props.Add("Imm_Parent_Country_Code", d.Imm_Parent_Country_Code);
            if (fields.Contains("Dom_Ult_AT_Key")) props.Add("Dom_Ult_AT_Key", d.Dom_Ult_AT_Key);
            if (fields.Contains("Dom_Ult_Name")) props.Add("Dom_Ult_Name", d.Dom_Ult_Name);
            if (fields.Contains("Dom_Ult_Street")) props.Add("Dom_Ult_Street", d.Dom_Ult_Street);
            if (fields.Contains("Dom_Ult_City")) props.Add("Dom_Ult_City", d.Dom_Ult_City);
            if (fields.Contains("Dom_Ult_State")) props.Add("Dom_Ult_State", d.Dom_Ult_State);
            if (fields.Contains("Dom_Ult_Zip")) props.Add("Dom_Ult_Zip", d.Dom_Ult_Zip);
            if (fields.Contains("Dom_Ult_Phone")) props.Add("Dom_Ult_Phone", d.Dom_Ult_Phone);
            if (fields.Contains("Dom_Ult_Country_Code")) props.Add("Dom_Ult_Country_Code", d.Dom_Ult_Country_Code);
            if (fields.Contains("Leg_Ult_Parent_AT_Key")) props.Add("Leg_Ult_Parent_AT_Key", d.Leg_Ult_Parent_AT_Key);
            if (fields.Contains("Leg_Ult_Parent_Name")) props.Add("Leg_Ult_Parent_Name", d.Leg_Ult_Parent_Name);
            if (fields.Contains("Leg_Ult_Parent_Street")) props.Add("Leg_Ult_Parent_Street", d.Leg_Ult_Parent_Street);
            if (fields.Contains("Leg_Ult_Parent_City")) props.Add("Leg_Ult_Parent_City", d.Leg_Ult_Parent_City);
            if (fields.Contains("Leg_Ult_Parent_State")) props.Add("Leg_Ult_Parent_State", d.Leg_Ult_Parent_State);
            if (fields.Contains("Leg_Ult_Parent_Zip")) props.Add("Leg_Ult_Parent_Zip", d.Leg_Ult_Parent_Zip);
            if (fields.Contains("Leg_Ult_Parent_Phone")) props.Add("Leg_Ult_Parent_Phone", d.Leg_Ult_Parent_Phone);
            if (fields.Contains("Leg_Ult_Parent_Country_Code")) props.Add("Leg_Ult_Parent_Country_Code", d.Leg_Ult_Parent_Country_Code);
            if (fields.Contains("Leg_Imm_Parent_AT_Key")) props.Add("Leg_Imm_Parent_AT_Key", d.Leg_Imm_Parent_AT_Key);
            if (fields.Contains("Leg_Imm_Parent_Name")) props.Add("Leg_Imm_Parent_Name", d.Leg_Imm_Parent_Name);
            if (fields.Contains("Leg_Imm_Parent_Street")) props.Add("Leg_Imm_Parent_Street", d.Leg_Imm_Parent_Street);
            if (fields.Contains("Leg_Imm_Parent_City")) props.Add("Leg_Imm_Parent_City", d.Leg_Imm_Parent_City);
            if (fields.Contains("Leg_Imm_Parent_State")) props.Add("Leg_Imm_Parent_State", d.Leg_Imm_Parent_State);
            if (fields.Contains("Leg_Imm_Parent_Zip")) props.Add("Leg_Imm_Parent_Zip", d.Leg_Imm_Parent_Zip);
            if (fields.Contains("Leg_Imm_Parent_Phone")) props.Add("Leg_Imm_Parent_Phone", d.Leg_Imm_Parent_Phone);
            if (fields.Contains("Leg_Imm_Parent_Country_Code")) props.Add("Leg_Imm_Parent_Country_Code", d.Leg_Imm_Parent_Country_Code);
            if (fields.Contains("Leg_Dom_Ult_AT_Key")) props.Add("Leg_Dom_Ult_AT_Key", d.Leg_Dom_Ult_AT_Key);
            if (fields.Contains("Leg_Dom_Ult_Name")) props.Add("Leg_Dom_Ult_Name", d.Leg_Dom_Ult_Name);
            if (fields.Contains("Leg_Dom_Ult_Street")) props.Add("Leg_Dom_Ult_Street", d.Leg_Dom_Ult_Street);
            if (fields.Contains("Leg_Dom_Ult_City")) props.Add("Leg_Dom_Ult_City", d.Leg_Dom_Ult_City);
            if (fields.Contains("Leg_Dom_Ult_State")) props.Add("Leg_Dom_Ult_State", d.Leg_Dom_Ult_State);
            if (fields.Contains("Leg_Dom_Ult_Zip")) props.Add("Leg_Dom_Ult_Zip", d.Leg_Dom_Ult_Zip);
            if (fields.Contains("Leg_Dom_Ult_Phone")) props.Add("Leg_Dom_Ult_Phone", d.Leg_Dom_Ult_Phone);
            if (fields.Contains("Leg_Dom_Ult_Country_Code")) props.Add("Leg_Dom_Ult_Country_Code", d.Leg_Dom_Ult_Country_Code);
            if (fields.Contains("Leg_Entity_Parent_AT_Key")) props.Add("Leg_Entity_Parent_AT_Key", d.Leg_Entity_Parent_AT_Key);
            if (fields.Contains("Leg_Entity_Parent_Name")) props.Add("Leg_Entity_Parent_Name", d.Leg_Entity_Parent_Name);
            if (fields.Contains("Leg_Entity_Parent_Street")) props.Add("Leg_Entity_Parent_Street", d.Leg_Entity_Parent_Street);
            if (fields.Contains("Leg_Entity_Parent_City")) props.Add("Leg_Entity_Parent_City", d.Leg_Entity_Parent_City);
            if (fields.Contains("Leg_Entity_Parent_State")) props.Add("Leg_Entity_Parent_State", d.Leg_Entity_Parent_State);
            if (fields.Contains("Leg_Entity_Parent_Zip")) props.Add("Leg_Entity_Parent_Zip", d.Leg_Entity_Parent_Zip);
            if (fields.Contains("Leg_Entity_Parent_Phone")) props.Add("Leg_Entity_Parent_Phone", d.Leg_Entity_Parent_Phone);
            if (fields.Contains("Leg_Entity_Parent_Country_Code")) props.Add("Leg_Entity_Parent_Country_Code", d.Leg_Entity_Parent_Country_Code);
            if (fields.Contains("Leg_Has_Child")) props.Add("Leg_Has_Child", d.Leg_Has_Child);
            if (fields.Contains("Leg_Link_Ind")) props.Add("Leg_Link_Ind", d.Leg_Link_Ind);
            if (fields.Contains("Leg_Org_Gp_Cd")) props.Add("Leg_Org_Gp_Cd", d.Leg_Org_Gp_Cd);
            if (fields.Contains("IndFrm")) props.Add("IndFrm", d.IndFrm);
            if (fields.Contains("Date_Established")) props.Add("Date_Established", d.Date_Established);
            if (fields.Contains("Stock_Sym")) props.Add("Stock_Sym", d.Stock_Sym);
            if (fields.Contains("Stock_Exch")) props.Add("Stock_Exch", d.Stock_Exch);
            if (fields.Contains("Num_Employees")) props.Add("Num_Employees", d.Num_Employees);
            if (fields.Contains("Loc_Num_Employees")) props.Add("Loc_Num_Employees", d.Loc_Num_Employees);
            if (fields.Contains("Bus_Type")) props.Add("Bus_Type", d.Bus_Type);
            if (fields.Contains("US_Citizen")) props.Add("US_Citizen", d.US_Citizen);
            if (fields.Contains("Disabled")) props.Add("Disabled", d.Disabled);
            if (fields.Contains("Veteran")) props.Add("Veteran", d.Veteran);
            if (fields.Contains("Web_Addr")) props.Add("Web_Addr", d.Web_Addr);
            if (fields.Contains("EMail")) props.Add("EMail", d.EMail);
            if (fields.Contains("Dead_Status")) props.Add("Dead_Status", d.Dead_Status);
            if (fields.Contains("Dead_Date")) props.Add("Dead_Date", d.Dead_Date);
            if (fields.Contains("Site_Tp")) props.Add("Site_Tp", d.Site_Tp);
            if (fields.Contains("Site_Tp_Desc")) props.Add("Site_Tp_Desc", d.Site_Tp_Desc);
            if (fields.Contains("Site_Tp_Cd")) props.Add("Site_Tp_Cd", d.Site_Tp_Cd);
            if (fields.Contains("Site_Ext_Cd_1")) props.Add("Site_Ext_Cd_1", d.Site_Ext_Cd_1);
            if (fields.Contains("Services_Areas")) props.Add("Services_Areas", d.Services_Areas);
            if (fields.Contains("W9_Name")) props.Add("W9_Name", d.W9_Name);
            if (fields.Contains("W9_Bus_Name")) props.Add("W9_Bus_Name", d.W9_Bus_Name);
            if (fields.Contains("EDI")) props.Add("EDI", d.EDI);
            if (fields.Contains("E_Commerce")) props.Add("E_Commerce", d.E_Commerce);
            if (fields.Contains("Referral")) props.Add("Referral", d.Referral);
            if (fields.Contains("Fiscal_Yr_End")) props.Add("Fiscal_Yr_End", d.Fiscal_Yr_End);
            if (fields.Contains("Tax_Exempt")) props.Add("Tax_Exempt", d.Tax_Exempt);
            if (fields.Contains("Tax_ID")) props.Add("Tax_ID", d.Tax_ID);
            if (fields.Contains("Tax_Type_ID")) props.Add("Tax_Type_ID", d.Tax_Type_ID);
            if (fields.Contains("CMSA")) props.Add("CMSA", d.CMSA);
            if (fields.Contains("CMSA_Desc")) props.Add("CMSA_Desc", d.CMSA_Desc);
            if (fields.Contains("PMSA")) props.Add("PMSA", d.PMSA);
            if (fields.Contains("PMSA_Desc")) props.Add("PMSA_Desc", d.PMSA_Desc);
            if (fields.Contains("CSA")) props.Add("CSA", d.CSA);
            if (fields.Contains("CSA_Desc")) props.Add("CSA_Desc", d.CSA_Desc);
            if (fields.Contains("CBSA")) props.Add("CBSA", d.CBSA);
            if (fields.Contains("CBSA_Desc")) props.Add("CBSA_Desc", d.CBSA_Desc);
            if (fields.Contains("CBSA_Type")) props.Add("CBSA_Type", d.CBSA_Type);
            if (fields.Contains("DIV")) props.Add("DIV", d.DIV);
            if (fields.Contains("DIV_Desc")) props.Add("DIV_Desc", d.DIV_Desc);
            if (fields.Contains("Congressional_District")) props.Add("Congressional_District", d.Congressional_District);
            if (fields.Contains("Client_Relationships")) props.Add("Client_Relationships", d.Client_Relationships);
            if (fields.Contains("Non_FIs")) props.Add("Non_FIs", d.Non_FIs);
            if (fields.Contains("EFX_Rating")) props.Add("EFX_Rating", d.EFX_Rating);
            if (fields.Contains("Marketability_Scores")) props.Add("Marketability_Scores", d.Marketability_Scores);


            return new DataEntity(EntityName) { Properties = props };
        }

        public static DetailsResponse Get(EquifaxCISDMWebServicesSoapClient client, DetailsRequest req)
        {
            return client.Details(req);
        }

        public static DetailsRequest CreateDetailsRequest(IDictionary<string, object> input, AuthHeader authHeader)
        {
            var EFX_ID = EntityExt.Get(input, "EFX_ID", -1);
            return new DetailsRequest(authHeader, EFX_ID);
        }

        public static IObjectDefinition Describe(IEnumerable<string> actions)
        {
            var requestType = typeof(DetailsRequest);
            var responseType = typeof(Detail);
            return PropertyExt.MakeCompoundType(
                requestType,
                responseType,
                EntityName,
                actions);
        }
    
    }
    

    public static class EntityExt
    {
        public static TVal GetOrDefault<TKey, TVal>(IDictionary<TKey, object> dic, TKey key)
        {
            object x;
            if (dic.TryGetValue(key, out x))
            {
                return (TVal)x;
            }
            else return default(TVal);
        }

        public static TVal GetOrDefault<TKey, TVal>(IDictionary<TKey, object> dic, TKey key, TVal defVal)
        {
            object x;
            if (dic.TryGetValue(key, out x))
            {
                return (TVal)x;
            }
            else return defVal;
        }

        public static TVal Get<TVal>(IDictionary<string, object> dic, string key, TVal defVal)
        {
            return GetOrDefault(dic, key, defVal);
        }

        public static TVal Get<TVal>(IDictionary<string, object> dic, string key)
        {
            return GetOrDefault<string, TVal>(dic, key);
        }
    }
}
