﻿namespace Equifax
{

    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Actions;
    using Scribe.Core.ConnectorApi.Query;

    using System;
    using System.Collections.Generic;

    using System.ServiceModel;
    using Equifax_SOAP.Equifax;
    using Scribe.Core.ConnectorApi.Exceptions;
    using System.Linq;

    using CDK.Entities;

    [ScribeConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(Connector),
        StandardConnectorSettings.SettingsUITypeName,
        StandardConnectorSettings.SettingsUIVersion,
        StandardConnectorSettings.ConnectionUITypeName,
        StandardConnectorSettings.ConnectionUIVersion,
        StandardConnectorSettings.XapFileName,
        new[] { "Scribe.IS2.Source", "Scribe.IS2.Target" },
        ConnectorSettings.SupportsCloud,
        ConnectorSettings.ConnectorVersion
        )]

     public class Connector : IConnector
    {

        public bool IsConnected { get; private set; }
        private readonly Guid connectorTypeId = new Guid(ConnectorSettings.ConnectorTypeId);
        private MetadataProvider metadataProvider;


        public string PreConnect(IDictionary<string, string> properties)
        {
            return ConnectionInfo.GetFormDefinition().Serialize();
        }

        private AuthHeader authHeader;
        private ConnectionInfo info;

        public void Connect(IDictionary<string, string> properties)
        {
            this.info = ConnectionInfo.Create(properties);
            var client = GetClient(this.info);
            this.authHeader = new AuthHeader { Username = this.info.UserName, Password = this.info.Password };
            try {
                //var request = this.CreateDiscoveryRequest();
                //var response = client.Discovery(request);
                this.IsConnected = true;

            }catch( Exception exn){
                this.IsConnected = false;
                throw new InvalidConnectionException("Probably did not have the correct Username and Password.");            }
       }

        private DiscoveryRequest CreateDiscoveryRequest()
        {
            var request = new DiscoveryRequest(
                this.authHeader,
                "Acme, Inc.",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                1,
                null,
                null);
            return request;
        }

        public void reconnect()
        {
            //Add Connection validation here

            this.IsConnected = true;
        }

        public void Disconnect()
        {
            this.IsConnected = false;
            this.metadataProvider = null;
        }

        public IMetadataProvider GetMetadataProvider()
        {
            this.metadataProvider = new MetadataProvider(this);
             
            return this.metadataProvider;
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            var root = query.RootEntity;
            var entity = root.ObjectDefinitionFullName;
            var inputParams = query.Constraints.GetParams().ToDictionary(kv => kv.Key, kv => kv.Value);

            switch (entity)
            {
                case DiscoveryE.EntityName:
                    return this.EqQueryDiscovery(inputParams, query.RootEntity.PropertyList);
                case DetailE.EntityName:
                    return this.EqQueryDetails(inputParams, query.RootEntity.PropertyList);

                default: throw new InvalidExecuteQueryException("Unrecognized Entity: " + entity);
            }
        }

        private static EquifaxCISDMWebServicesSoapClient GetClient(ConnectionInfo ci)
        {
            var binding = new BasicHttpBinding { Security = { Mode = BasicHttpSecurityMode.Transport } };

            var remoteAddress = new EndpointAddress(ci.BaseURL.Replace("?wsdl", ""));

            var client = new EquifaxCISDMWebServicesSoapClient(binding, remoteAddress);
            return client;
        }

        private IEnumerable<DataEntity> EqQueryDiscovery(IDictionary<string, object> input, IList<string> fields)
        {
            var client = GetClient(this.info);

            try
            {
                var request = DiscoveryE.CreateDiscoveryRequest(input, this.authHeader);
                var response = DiscoveryE.Get(client, request);
                return response.ToDataEntities(fields);// .DiscoveryResult.Select(r => DiscoveryE.DiscoveryResultToDataEntity(r, fields));
            }
            catch (Exception exn)
            {
                throw new InvalidExecuteQueryException(exn.Message);
            }
        }

        private IEnumerable<DataEntity> EqQueryDetails(IDictionary<string, object> inp, IList<string> fields)
        {
            var client = GetClient(this.info);
            Detail d;
            try
            {
                var request = DetailE.CreateDetailsRequest(inp, this.authHeader);
                d = client.Details(request).DetailsResult;
            }
            catch (Exception exn)
            {
                throw new InvalidExecuteQueryException(exn.Message);
            }

            yield return DetailE.DetailResultToDataEntity(d, fields);
        }
        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();

        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            throw new NotImplementedException();
        }
        public Guid ConnectorTypeId
        {
            get
            {
                return connectorTypeId;
            }
        }
    }
}
