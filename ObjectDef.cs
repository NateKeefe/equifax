namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Scribe.Core.ConnectorApi.Metadata;

    public class ObjectDef : IObjectDefinition
    {
        private readonly string name;

        private readonly string description;

        private readonly bool hidden = true;

        private readonly List<string> supportedActionFullNames;

        private readonly List<IPropertyDefinition> propertyDefinitions;

        private readonly List<IRelationshipDefinition> relationshipDefinitions = new List<IRelationshipDefinition>();

        public ObjectDef(string name, string description, IEnumerable<IPropertyDefinition> props, IEnumerable<string> supportedActions)
        {
            this.name = name;
            this.description = description;
            this.supportedActionFullNames = new List<string>(supportedActions);
            this.propertyDefinitions = props.ToList();

        }

        string IObjectDefinition.FullName
        {
            get
            {
                return this.name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IObjectDefinition.Name
        {
            get
            {
                return this.name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IObjectDefinition.Description
        {
            get
            {
                return this.description;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IObjectDefinition.Hidden
        {
            get
            {
                return this.hidden;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        List<string> IObjectDefinition.SupportedActionFullNames
        {
            get
            {
                return this.supportedActionFullNames;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        List<IPropertyDefinition> IObjectDefinition.PropertyDefinitions
        {
            get
            {
                return this.propertyDefinitions;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        List<IRelationshipDefinition> IObjectDefinition.RelationshipDefinitions
        {
            get
            {
                return this.relationshipDefinitions;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}