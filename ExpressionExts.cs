namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Scribe.Core.ConnectorApi;

    using Expression = System.Linq.Expressions.Expression;
    using ExpressionType = Scribe.Core.ConnectorApi.ExpressionType;

    public static class ScribeExpressionExt
    {
        // This is just to get things rolling (only do All ANDS)
        public static IEnumerable<KeyValuePair<string, object>> GetParams(this ComparisonExpression comparison)
        {
            if (comparison.Operator == ComparisonOperator.Equal)
            {
                var leftValAsString = comparison.LeftValue.Value.ToString();
                var lv = comparison.LeftValue.ValueType == ComparisonValueType.Property
                             ? leftValAsString.Split('.')[leftValAsString.Split('.').Length - 1]
                             : leftValAsString;

                yield return new KeyValuePair<string, object>(lv, comparison.RightValue.Value.ToString());
            }
            else
            {
                var operatorAsString = comparison.Operator.ToString();

                var leftValAsString = comparison.LeftValue.Value.ToString();
                var lv = comparison.LeftValue.ValueType == ComparisonValueType.Property
                             ? leftValAsString.Split('.')[leftValAsString.Split('.').Length - 1]
                             : leftValAsString;
                var lvWithOperator = lv + "|" + operatorAsString;

                yield return new KeyValuePair<string, object>(lvWithOperator, comparison.RightValue.Value.ToString());
            }
        }

        // This is just to get things rolling (only do All ANDS)
        public static IEnumerable<KeyValuePair<string, object>> GetParams(this LogicalExpression logical)
        {
            return logical.Operator == LogicalOperator.And
                       ? logical.LeftExpression.GetParams().Concat(logical.RightExpression.GetParams())
                       : Enumerable.Empty<KeyValuePair<string, object>>();
        }

        public static Expression Convert(Scribe.Core.ConnectorApi.Expression expr, Expression<bool> previous = null)
        {
            // System.Linq.Expressions.Expression<bool> b;
            var exprType = expr.ExpressionType;

            switch (exprType)
            {
                case ExpressionType.Logical:
                    break;
                case ExpressionType.Comparison:
                    if (previous == null)
                    {
                        return Convert(expr as ComparisonExpression);
                    }

                    break;
                case ExpressionType.Arithmetic:
                case ExpressionType.Assignment:
                case ExpressionType.Value:
                case ExpressionType.Function:
                case ExpressionType.Ternary:
                case ExpressionType.Condition:
                case ExpressionType.Group:
                case ExpressionType.Lambda:
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return previous;
        }

        public static Expression FromExpression(ComparisonValue v)
        {
            var vt = v.ValueType;
            switch (vt)
            {
                case ComparisonValueType.Property:
                    return Expression.Constant(v.Value.ToString());
                case ComparisonValueType.Constant:
                    return Expression.Constant(v.Value);
                case ComparisonValueType.QueryEntity:
                case ComparisonValueType.Variable:
                case ComparisonValueType.Formula:
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static Expression Convert(ComparisonExpression expr)
        {
            var op = expr.Operator;
            var lh = expr.LeftValue;
            var rh = expr.RightValue;
            switch (op)
            {
                case ComparisonOperator.Equal:
                    return Expression.Equal(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.NotEqual:
                    return Expression.NotEqual(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.Less:
                    return Expression.LessThan(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.LessOrEqual:
                    return Expression.LessThanOrEqual(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.Greater:
                    return Expression.GreaterThan(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.GreaterOrEqual:
                    return Expression.GreaterThanOrEqual(FromExpression(lh), FromExpression(rh));
                case ComparisonOperator.IsNull:
                    return Expression.Equal(FromExpression(lh), Expression.Constant(null));
                case ComparisonOperator.IsNotNull:
                    return Expression.NotEqual(FromExpression(lh), Expression.Constant(null));
                case ComparisonOperator.Like:
                    throw new ArgumentOutOfRangeException("The Like operator is not supported in this version.");
                case ComparisonOperator.NotLike:
                    throw new ArgumentOutOfRangeException("The NotLike operator is not supported in this version.");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static IEnumerable<KeyValuePair<string, object>> GetParams(this Scribe.Core.ConnectorApi.Expression both)
        {
            if (both == null)
            {
                return Enumerable.Empty<KeyValuePair<string, object>>();
            }

            var l = both as LogicalExpression;
            if (l != null)
            {
                return l.GetParams();
            }

            var c = both as ComparisonExpression;
            if (c != null)
            {
                return c.GetParams();
            }

            throw new Exception(string.Format("Unexpected Expression type {0}.", both.ExpressionType));
        }

        public static IDictionary<TK, TV> ToDictionary<TK, TV>(this IEnumerable<KeyValuePair<TV, TK>> source)
        {
            return source.ToDictionary(s => s.Key, s => s.Value) as IDictionary<TK, TV>;
        }
    }
}