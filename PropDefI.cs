namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PropDefI
    {
        public PropDefI(string name, Type pType, PUse puse)
        {
            this.Name = name;
            this.PType = pType;
            this.Use = puse;
        }

        public string Name { get; }
        public Type PType { get; }
        public PUse Use { get; }

        public static PropDefI Merge(PropDefI pda, IEnumerable<PropDefI> pdbs)
        {
            // Actually only expecting bs to be a list of 1, but ...
            return pdbs.Aggregate(pda, Combine);
        }

        public static PropDefI Combine(PropDefI a, PropDefI b)
        {
            var name = a.Name; // Always use the first name - they should be the same
            var t = Embiggen(a.PType, b.PType);
            var u = PUse.Merge(a.Use, b.Use);
            return new PropDefI(name, t, u);
        }

        public static Type Embiggen(Type pType, Type type)
        {
            if (pType == type) return pType;
            return pType.IsAssignableFrom(type) ? pType : type;
        }
    }
}