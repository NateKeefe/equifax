namespace Equifax
{
    using System;

    using Scribe.Core.ConnectorApi.Metadata;

    public class PropDef : IPropertyDefinition
    {
        public PropDef(string name, Type propertyType, PUse puse)
        {
            this.name = name;
            this.propertyType = propertyType.FullName;
            this.puse = puse;
        }
        private readonly string name;

        private readonly string propertyType;

        private readonly bool nullable = true;

        private readonly bool isPrimaryKey = false;

        private readonly PUse puse;

        string IPropertyDefinition.FullName
        {
            get
            {
                return this.name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IPropertyDefinition.Name
        {
            get
            {
                return this.name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IPropertyDefinition.Description
        {
            get
            {
                return $"The { this.name } property.";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IPropertyDefinition.PropertyType
        {
            get
            {
                return this.propertyType;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IPropertyDefinition.MinOccurs
        {
            get
            {
                return 1;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IPropertyDefinition.MaxOccurs
        {
            get
            {
                return 1;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IPropertyDefinition.Size
        {
            get
            {
                return 0;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IPropertyDefinition.NumericScale
        {
            get
            {
                return 0;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IPropertyDefinition.NumericPrecision
        {
            get
            {
                return 0;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IPropertyDefinition.PresentationType
        {
            get
            {
                return this.propertyType;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.Nullable
        {
            get
            {
                return this.nullable;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.IsPrimaryKey
        {
            get
            {
                return this.isPrimaryKey;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInQuerySelect
        {
            get
            {
                return this.puse.UsedInQuerySelect;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInQueryConstraint
        {
            get
            {
                return this.puse.UsedInQueryConstraint;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInActionInput
        {
            get
            {
                return this.puse.UsedInActionInput;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInActionOutput
        {
            get
            {
                return this.puse.UsedInActionOutput;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInLookupCondition
        {
            get
            {
                return this.puse.UsedInLookupCondition;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.UsedInQuerySequence
        {
            get
            {
                return this.puse.UsedInQuerySequence;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IPropertyDefinition.RequiredInActionInput
        {
            get
            {
                return this.puse.RequiredInActionInput;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}