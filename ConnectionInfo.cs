﻿namespace Equifax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Scribe.Core.ConnectorApi.ConnectionUI;
    using Scribe.Core.ConnectorApi.Cryptography;

    public class ConnectionInfo
    {
        public ConnectionInfo(string baseUrl, string username, string password)
        {
            this.BaseURL = baseUrl;
            this.UserName = username;
            this.Password = password;
        }
        public static string BaseUrlName = "BaseURL";
        public string BaseURL { get; }

        public static string UserNameName = "UserName";
        public string UserName { get;}

        public static string PasswordName = "Password";
        public string Password { get;}

        public static ConnectionInfo Create(IDictionary<string, string> dic)
        {
            var url = dic[BaseUrlName];
            var uname = dic[UserNameName];
            var pw = Decryptor.Decrypt_AesManaged(dic[PasswordName], ConnectorSettings.cryptoKey);

            return new ConnectionInfo(url, uname, pw);
        }

        public static FormDefinition GetFormDefinition()
        {
            return new FormDefinition
                       {
                           CompanyName = "Equifax",
                           CryptoKey = ConnectorSettings.cryptoKey,
                           HelpUri = new Uri("http://www.scribesoft.com"),
                           Entries =
                               new Collection<EntryDefinition>
                                   {
                                       new EntryDefinition
                                           {
                                               InputType = InputType.Text,
                                               IsRequired = true,
                                               Label = "API URL",
                                               PropertyName = ConnectionInfo.BaseUrlName
                                           },
                                       new EntryDefinition
                                           {
                                               InputType = InputType.Text,
                                               IsRequired = true,
                                               Label = "User Name",
                                               PropertyName = ConnectionInfo.UserNameName
                                           },
                                       new EntryDefinition
                                           {
                                               InputType = InputType.Password,
                                               IsRequired = true,
                                               Label = "Password",
                                               PropertyName = ConnectionInfo.PasswordName
                                           },
                                   }
                       };
        }
    }
}
